# As every model we create is going to extend the base class
import datetime
from enum import unique
import sqlalchemy
from sqlalchemy.sql.expression import text
from .database import Base
from sqlalchemy import Column, ForeignKey,String,Integer,Boolean,TIMESTAMP,DateTime

# class Post(Base):
#     __tablename__ = "posts_orm"
#     id = Column(Integer,primary_key=True,nullable=False)
#     title = Column(String,nullable=False)
#     content = Column(String, nullable=False)
#     published = Column(Boolean,default=True)

posts_orm = sqlalchemy.Table(
    "posts_orm",
    Base.metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True,nullable=False),
    sqlalchemy.Column("title", sqlalchemy.String,nullable=False),
    sqlalchemy.Column("content", sqlalchemy.String,nullable=False),
    sqlalchemy.Column("published", sqlalchemy.Boolean,default=True),
    sqlalchemy.Column('created_at',sqlalchemy.TIMESTAMP(timezone=True), nullable=False,server_default = text('now()')),
    sqlalchemy.Column("owner_id",sqlalchemy.Integer,sqlalchemy.ForeignKey("users.id",ondelete="CASCADE"),nullable=False)

)


class User(Base):
    __tablename__= "users"
    id = Column(Integer,primary_key = True, nullable= False)
    email = Column(String, nullable=False,unique=True)
    password = Column(String,nullable = False)
    
    created_at = Column(TIMESTAMP(timezone=True),nullable=False,server_default = text('now()'))

class EmployeeDetails(Base):
    __tablename__ = "employee_details"
    id = Column(Integer,primary_key=True, nullable=False)
    first_name = Column(String,nullable=False)
    last_name = Column(String,nullable=False)
    address = Column(String,nullable=False)
    email = Column(String,nullable=False)
    phone_number = Column(String,nullable=False)