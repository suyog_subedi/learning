from pydantic import BaseModel
from typing import Optional


class PostSchema(BaseModel):
    title: str
    content: str
    owner_id:int
    published:Optional[bool]=False


class PostDelete(BaseModel):
    id:int   


class UserCreate(BaseModel):
    email:str
    password:str

class UserResponse(BaseModel):
    email:str

class UserLogin(BaseModel):
    email:str
    password:str

class AddToCSV(BaseModel):
    first:str
    last:str
    address:str
    email:str
    phone:str