from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "postgresql://postgres:root@localhost/starlette"

# Engine is responsible for sqlalchemy to establish a connection with the database
engine = create_engine(SQLALCHEMY_DATABASE_URL)

# Engine establishes a connection but to talk with the database we need to have a session
SessionLocal = sessionmaker(autocommit=False, autoflush=False,bind=engine)

# All the models we create using sqlalchemy will be extending using Base Class
Base = declarative_base()