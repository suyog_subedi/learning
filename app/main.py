
import email
import os
from typing import final
from unittest import result
import databases
from psycopg2 import IntegrityError
from sqlalchemy import delete
import csv
from . import utils

from starlette.applications import Starlette
from starlette.responses import JSONResponse, PlainTextResponse
from starlette.exceptions import HTTPException
# Pydantic
from pydantic import ValidationError
from .schemas import PostDelete,PostSchema,UserCreate,UserResponse

# For the db connection
from . database import SessionLocal
from . import models
from .models import posts_orm,User
from . import schemas


from . database import engine
import json
from sqlalchemy.sql import select

# middlewares
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

# For Documentation
from apiman.starlette import Apiman
apiman = Apiman(template="./docs/openapi.yml")
# for password hashing
from passlib.context import CryptContext
# telling passlib that we want to use bcrypt hashing algorithm
pwd_context = CryptContext(schemes=["bcrypt"],deprecated="auto")
db = SessionLocal()

import pandas as pd
middlewares = [
    Middleware(CORSMiddleware,allow_origins=['*'])
]
# To create the models 
models.Base.metadata.create_all(bind=engine)
# this initiates the session
database = databases.Database("postgresql://postgres:root@localhost/starlette")
app = Starlette(on_startup=[database.connect],on_shutdown=[database.disconnect],middleware=middlewares)
apiman.init_app(app)


# -----------
# Insomnia 
# OSO HQ
# Cosmic Python
# Decorator and generator
# some sort of calculation in api endpoint
# Comprehension
# yield

# -------------------_First-----------------
# csv ko data lai db ma halne

# To get to the api documentation
# http://127.0.0.1:8000/apiman/swagger/



@app.route("/", methods=["GET"])
async def homepage(request):
    """
    ---
    summary: Homepage of the application
    tags:
    - Homepage
    responses:
      "200":
        description: OK
    """
 
    return JSONResponse({'hello': 'Homepage'})

    
@app.route("/posts",methods=["GET"])
async def get_all_posts(request):
    """
    ---
    summary: To get all the posts
    tags:
    - Posts
    responses:
      "200":
        description: OK
    """
    
    query = posts_orm.select()
    results = await database.fetch_all(query=query)
   
    
    content =[
        {
            "id": result['id'],
            "title":result['title'],
            "content":result['content'],
            "published":result['published']

        }
        for result in results
    ]
    return JSONResponse({"data" : content})
      




# # New Post
@app.route("/posts", methods=["POST"])
async def create_posts(request):
    """
    ---
      summary: Create a new post
      tags:
      - Posts
      parameters:
      - name: title
        type: string
        in: path
        required: True
      - name: content
        type: string
        in: path
        required: True
      - name: owner_id
        type: integer
        in: path
        required: True
    
      responses:
        "201":
          description: OK
        "409":
          description: Conflict
    """
    data = await request.json()
    try:
        data_validation = PostSchema(**data)
        data_dict = data_validation.dict()
        query = posts_orm.insert().values(data_dict)
        await database.execute(query)
        return JSONResponse({"data":data_dict})
    except ValidationError as e:
        print("I don't like the way, you are sending data")
        return JSONResponse({"error": e.json()})

# Get Single POST
@app.route("/posts/{id}",methods=["GET"])
async def get_single_post(request):
    """
    ---

      summary: Get a new post by id
      tags:
      - Posts
      parameters:
      - name: id
        type: string
        in: path
        required: True
      responses:
        "200":
          description: OK
        "404":
          description: Not found
    """
    id = str(request.path_params['id'] ) 
    # c is also needed to get the values
    query = select(posts_orm).where(posts_orm.c.id ==id)
    post = db.execute(query)
    

    content = [
        {
            "id":p['id'],
            "title":p['title'],
            "content":p['content']
        }
        for p in post
    ]
    if len(content)==0:
        return JSONResponse({"data":f"The post with id {id} does not exist"})
    return JSONResponse({"data":content})
   
    

# Update Post
@app.route("/posts/{id}",methods=["PUT"])
async def update_post(request):
    """
    ---
      summary: Update a post
      tags:
      - Posts
      parameters:
      - name: title
        type: string
        in: path
        required: True
      - name: content
        type: string
        in: path
        required: True
      - name: owner_id
        type: integer
        in: path
        required: True
    
      responses:
        "204":
          description: Update Successfully
        "409":
          description: Conflict
    """
    id = str(request.path_params['id'])
    data = await request.json()
    try:
        data_validation = PostSchema(**data)
        data_dict = data_validation.dict()
        print(data_dict['title'],data_dict["content"],data_dict['published'])
        query = posts_orm.update().where(posts_orm.c.id==id).values(title=data_dict['title'],content=data_dict['content'],published=data_dict['published'])
        post = db.execute(query)
        db.commit()
        return JSONResponse({"msg":"The data has been updated"})
    except ValidationError as e:
        print(e)
        return JSONResponse({"msg":"Seems like there is some error"})


@app.route("/posts/{id}",methods=["DELETE"])
async def delete_post(request):
    """
    ---
      summary: Delete a post
      tags:
      - Posts
      parameters:
      - name: id
        type: string
        in: path
        required: True
      responses:
        "200":
          description: OK
        "204":
          description: Not found
    """
    id = request.path_params['id']
    try:
        PostDelete(id=id)
        query=delete(posts_orm).where(posts_orm.c.id==id)
        deleting = db.execute(query)
        db.commit()
        return JSONResponse({"msg":"Deleted"})
    except ValidationError as e:
      
        return JSONResponse({"msg":"Encountered a problem"})
   
    

# ---------------------------------Users-----------------------------

@app.route("/users", methods=["POST"])
async def create_users(request):
    """
    ---
      summary: Create a new user
      tags:
      - Users
      parameters:
      - name: email
        type: string
        in: path
        required: True
      - name: password
        type: string
        in: path
        required: True
      responses:
        "201":
          description: OK

    """
    data = await request.json()
    try:
        data_validation =UserCreate(**data)
        password = utils.hash(data_validation.password)
        data_validation.password = password
        new_user = User(**data_validation.dict())
        try:
            db.add(new_user)
            db.commit()
        except Exception as e:
            return JSONResponse({"msg":"Email already in use"})
            

        return JSONResponse(UserResponse(email=data_validation.email).dict())
    except ValidationError as e:
        return JSONResponse({"msg":e})

@app.route("/users/{id}", methods=["GET"])
async def get_user(request):
    """
    ---
      summary: Get a user by id
      tags:
      - Users
      parameters:
      - name: id
        type: string
        in: path
        required: True
      responses:
        "200":
          description: OK
        "404":
          description: Not found
    """
    id = request.path_params['id']
    user_info = db.query(models.User).filter(models.User.id == id).first()
    print(user_info.id)
    print(type(type(user_info.id)))
    
    if not user_info:
        raise HTTPException(status_code=404)

    return JSONResponse({"id":user_info.id,"email":user_info.email})


# ----------------------------------------Authentication---------------
@app.route('/login',methods=["POST"])
async def login(request):
    """
    ---
      summary: Authentication
      tags:
      - Authentication
      parameters:
      - name: email
        type: string
        in: path
        required: True
      - name: password
        type: string
        in: path
        required: True
      responses:
        "401":
          description: Unauthorized
    """
    credentials = await request.json()
    
    data_validation = schemas.UserLogin(**credentials)
    user=db.query(models.User).filter(models.User.email==credentials['email']).first()

    if not user:
        return JSONResponse("Wrong Credentials")
    if not utils.verify(data_validation.password,user.password):
        return JSONResponse("Wrong Credentials")
    return JSONResponse({"token":"Your Token"})


@app.route('/csv',methods=["GET","POST"])
async def read_csv(request):
  # ----------------------------For adding data to the csv---------------------------------------- 
  if request.method == "POST":
    data = await request.json()
    try:
      data_validation =schemas.AddToCSV(**data)
      data_validation =data_validation.dict()
      final_data = list(data_validation.values())
      print(final_data[3])
      df = pd.read_csv(f'{os.getcwd()}/app/csv_files/sample_csv.csv',header=0)
      # Writing the value to csv
      with open(f'{os.getcwd()}/app/csv_files/sample_csv.csv','a') as f:
        writer = csv.writer(f)
        writer.writerow(final_data)
      return JSONResponse("Data Validated and Inserted to csv")
    except ValidationError as e:
      print(e)
      return JSONResponse("All the fields are required")

  # ----------------------------For adding csv data to the database----------------------------------------
  df = pd.read_csv(f'{os.getcwd()}/app/csv_files/sample_csv.csv',header=0)
  # Replacing the empty values with 0
  df = df.fillna('0')


  # Finding duplicate data
  df = df.drop_duplicates()
  duplicates= df.duplicated()
  print(df[duplicates])
  # Renaming all the columns of the csv file
  print("CSV files column name changed to match the column name in the database")
  df.columns=['first_name','last_name','address','email','phone_number']
  # Converting the df to sql format and adding to the database
  df.to_sql(con=engine,name = models.EmployeeDetails.__tablename__,if_exists='append',index=False)

  # Getting the sql data from the databse and showing it as a json in the frontend
  data = db.query(models.EmployeeDetails).all()

  content =[
    {
        "id":j.id,
        "first_name": j.first_name,
        "last_name":j.last_name,
        "address": j.address,
        "email":j.email,
        'phone_number':j.phone_number

    }
    for j in data
]
  return JSONResponse({"data":content})
