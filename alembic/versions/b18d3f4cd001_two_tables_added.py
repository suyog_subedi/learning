"""Two tables added

Revision ID: b18d3f4cd001
Revises: 
Create Date: 2022-06-02 09:23:17.720662

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b18d3f4cd001'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
